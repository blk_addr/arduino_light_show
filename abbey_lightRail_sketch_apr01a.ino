//Arduino light show with 10-LED light strip
//Made with lots of help from my 5 yr old

#include <PinChangeInt.h>


const int y1 = 11;
const int y2 = 10;
const int y3 = 9;
const int o1 = 8;
const int o2 = 7;
const int o3 = 6;
const int r1 = 5;
const int r2 = 4;
const int r3 = 3;
const int r4 = 2;

//Button setup, cycles through light shows
const int b1 = 12; //button pin
const int b1Max = 11; //max light shows -1
int b1Count = 0;
int b1Prev = -1; //tracks what the previous light show was
                 //in case it has a sepecific finish 

const int cycleMax = 15; //Max cycles if button not pressed
int cycleCount = 0; //cycle counter

long millisSinceLastCycle = 0;

void setup() {
  Serial.begin(9600);
  daddySetup();
  pinMode(b1, INPUT);
  digitalWrite(b1, HIGH);
  PCintPort::attachInterrupt(b1, b1Counter, RISING); //monitor button
}

void loop() {
  //don't keep it static for too long - switch it up a lil
  //if (cycleCount > cycleMax && b1Count == b1Prev) {
  //  b1Counter();
  //}
  long diff = millis() - millisSinceLastCycle;
  if ((millis() - millisSinceLastCycle) > 10000) {
    Serial.print("millis - millisSinceLastCycle = ");
    Serial.println(diff);
    b1Counter();
  }
  
  Serial.print("Button count at ");
  Serial.println(b1Count);
  if (b1Count != b1Prev) {
    switch (b1Prev) {
      case 0:
        daddyLightShow2(y1, o2, -1, LOW, o3, r4, -1, LOW, 50);
        break;
      case 1:
        daddyLightShow2(o2, y1, 1, LOW, r4, o3, 1, HIGH, 50);
        break;
      default:
        allOff();
        //delay(500);
        break;
    }
    
    b1Prev = b1Count;
    cycleCount = 0;
    millisSinceLastCycle = millis();
  }
  
  switch (b1Count) {
    case 0:
      wormLightShow();
      break;
    case 1:
      revWormLightShow();
      break;
    case 2:
      waWaLightShow();
      break;
    case 3:
      revWaWaLightShow();
      break;
    case 4:
      //slowLtoRLightShow();
      fastLtoRLightShow();
      break;
    case 5:
      fastRtoLLightShow();
      break;
    case 6:
      collideLightShow();
      break;
    case 7:
      runAwayLightShow();
      break;
    case 8:
      singleBlinkLightShow();
      break;
    case 9:
      altBlinkLightShow();
      break;
    case 10:
      tripleAltBlinkLightShow();
      break;
    case 11:
      revTripleAltBlinkLightShow();
      break;
    default:
      revWormLightShow();
      break;
  }
  
  cycleCount++;
}

void abbeySetup() {
  pinMode(y1, OUTPUT);
  pinMode(y2, OUTPUT);
  pinMode(y3, OUTPUT);
  pinMode(o1, OUTPUT);
  pinMode(o2, OUTPUT);
  pinMode(o3, OUTPUT);
  pinMode(r1, OUTPUT);
  pinMode(r2, OUTPUT);
  pinMode(r3, OUTPUT);
  pinMode(r4, OUTPUT);
  
}

void daddySetup() {
  for (int i = 11; i>=2; i--) {
    pinMode(i, OUTPUT);
  }
}

//Currently requires evenly divisible start&endPin
void daddyLightShow(int startPin, int endPin, long delayTime, int inc, int stat) {
  for (int i = startPin; i != endPin, i = i + inc;) {
    //Serial.print("Setting pin ");
    //Serial.print(i);
    //Serial.print(" to ");
    //Serial.println(stat);
    if(i == endPin || i < r4 || i > y1) {
      break;
    }
    if(i >= r4 && i <= y1) {
      digitalWrite(i, stat);
    }
    if (delayTime > 0) {
      delay(delayTime);
    }
  }
}

void daddyLightShow2(int startPin1, int endPin1, int inc1, int stat1,
   int startPin2, int endPin2, int inc2, int stat2, long delayTime) {
  
  int pin1 = startPin1;
  int pin2 = startPin2;
  
  while(!lightShow2Done(startPin1, endPin1, startPin2, endPin2, pin1, pin2)) {
  //while((pin1 >= r4 && pin1 <= y1) || (pin2 >= r4 && pin2 <= y1)
  //     || !lightShow2Done(startPin1, endPin1, startPin2, endPin2, pin1, pin2)) {
  //while(!lightShow2Done2(startPin1, endPin1, startPin2, endPin2, pin1, pin2)) {
    //digitalWrite(pin1, stat1);
    //digitalWrite(pin2, stat2);
    if(pin1 >= r4 && pin1 <= y1) {
      digitalWrite(pin1, stat1);
    }
    if(pin2 >= r4 && pin2 <= y1) {
      digitalWrite(pin2, stat2);
    }
    pin1 = pin1 + inc1;
    pin2 = pin2 + inc2;
    delay(delayTime);
  }
}
/*
  Function to determine if dual-pin light show is complete
*/
boolean lightShow2Done(int startPin1, int endPin1, int startPin2, 
      int endPin2, int pin1, int pin2) {
  
  boolean ret = false;
  ret = (pin1 < endPin1 && startPin1 > endPin1) ||
    (pin1 > endPin1 && startPin1 < endPin1) ||
    (pin2 < endPin2 && startPin2 > endPin2) ||
    (pin2 > endPin2 && startPin2 < endPin2);
  
  return ret;
}
boolean lightShow2Done2(int startPin1, int endPin1, int startPin2, 
      int endPin2, int pin1, int pin2) {
  
  boolean ret = false;
  ret = ((pin1 <= endPin1 && startPin1 >= endPin1) &&
    (pin2 <= endPin2 && startPin2 >= endPin2)) ||
    ((pin1 >= endPin1 && startPin1 <= endPin1) &&
    (pin2 >= endPin2 && startPin2 <= endPin2));
}

/*
  Different light shows
*/
void allOff() {
  daddyLightShow(r4 - 1, y1 + 1, 0, 1, LOW);
}
void wormLightShow() {
  daddyLightShow2(y1, o2, -1, HIGH, o3, r4, -1, LOW, 50);
  delay(50);
  daddyLightShow2(y1, o2, -1, LOW, o3, r4, -1, HIGH, 50);
  delay(50);
}
void revWormLightShow() {
  daddyLightShow2(o2, y1, 1, LOW, r4, o3, 1, HIGH, 50);
  delay(50);
  daddyLightShow2(o2, y1, 1, HIGH, r4, o3, 1, LOW, 50);
}
void waWaLightShow() {
  daddyLightShow2(o2, y1, 1, HIGH, o3, r4, -1, HIGH, 25);
  delay(25);
  daddyLightShow2(y1, o2, -1, LOW, r4, o3, 1, LOW, 25);
  delay(25);
}
void revWaWaLightShow() {
  daddyLightShow2(y1, o2, -1, HIGH, r4, o3, 1, HIGH, 25);
  delay(25);
  daddyLightShow2(o2, y1, 1, LOW, o3, r4, -1, LOW, 25);
  delay(25);
}
void slowLtoRLightShow() {
  //int startPin, int endPin, long delayTime, int inc, int stat
  daddyLightShow(y1 + 1, r4 - 1, 500, -1, HIGH);
  delay(500);
  daddyLightShow(r4 - 1, y1 + 1, 500, 1, LOW);
  delay(500);
}
void slowRtoLLightShow() {
  daddyLightShow(r4 - 1, y1 + 1, 500, 1, HIGH);
  delay(500);
  daddyLightShow(y1 + 1, r4 - 1, 500, -1, LOW);
  delay(500);
}
void fastLtoRLightShow() {
  daddyLightShow(y1 + 1, r4 - 1, 50, -1, HIGH);
  delay(50);
  daddyLightShow(r4 - 1, y1 + 1, 50, 1, LOW);
  delay(50);
}
void fastRtoLLightShow() {
  //int startPin, int endPin, long delayTime, int inc, int stat
  daddyLightShow(r4 - 1, y1 + 1, 50, 1, HIGH);
  delay(50);
  daddyLightShow(y1 + 1, r4 - 1, 50, -1, LOW);
  delay(50);
}
void collideLightShow() {
  daddyLightShow2(y1, o2, -1, HIGH, r4, o3, 1, HIGH, 50);
  delay(50);
  daddyLightShow2(y1, o2, -1, LOW, r4, o3, 1, LOW, 50);
  delay(50);
}
void runAwayLightShow() {
  daddyLightShow2(o2, y1, 1, HIGH, o3, r4, -1, HIGH, 50);
  delay(50);
  daddyLightShow2(o2, y1, 1, LOW, o3, r4, -1, LOW, 50);
  delay(50);
}
void singleBlinkLightShow() {
  //int startPin, int endPin, long delayTime, int inc, int stat
  daddyLightShow(y1+1, r4-1, 0, -1, HIGH);
  delay(250);
  daddyLightShow(y1+1, r4-1, 0, -1, LOW);
  delay(250);
}
void altBlinkLightShow() {
  daddyLightShow(y1+1, r4-1, 0, -2, HIGH);
  daddyLightShow(y1, r4, 0, -2, LOW);
  delay(100);
  allOff();
  daddyLightShow(y1+1, r4-1, 0, -2, LOW);
  daddyLightShow(y1, r4, 0, -2, HIGH);
  delay(100);
  allOff();
}
void tripleAltBlinkLightShow() {
  daddyLightShow(y1+3, r4-3, 0, -3, LOW);
  daddyLightShow(y1+2, r4-2, 0, -3, HIGH);
  delay(100);
  allOff();
  daddyLightShow(y1+2, r4-2, 0, -3, LOW);
  daddyLightShow(y1+1, r4-1, 0, -3, HIGH);
  delay(100);
  allOff();
  daddyLightShow(y1+1, r4-1, 0, -3, LOW);
  daddyLightShow(y1+3, r4-3, 0, -3, HIGH);
  delay(100);
  allOff();
}
void revTripleAltBlinkLightShow() {
  daddyLightShow(r4-3, y1+3, 0, +3, LOW);
  daddyLightShow(r4-2, y1+2, 0, +3, HIGH);
  delay(100);
  allOff();
  daddyLightShow(r4-2, y1+2, 0, +3, LOW);
  daddyLightShow(r4-1, y1+1, 0, +3, HIGH);
  delay(100);
  allOff();
  daddyLightShow(r4-1, y1+1, 0, +3, LOW);
  daddyLightShow(r4-3, y1+3, 0, +3, HIGH);
  delay(100);
  allOff();
}

void b1Counter() {
  //THANK YOU http://forum.arduino.cc/index.php/topic,45000.0.html
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  if (interrupt_time - last_interrupt_time > 200) {
    b1Count++;
    if (b1Count > b1Max) {
      b1Count = 0;
    }
  }
  last_interrupt_time = interrupt_time;
}
